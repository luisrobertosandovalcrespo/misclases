
package misclases;

public class Cotizacion {
    private int numCotizacion;
    private String descripcionAuto;
     private float precio;
    private float porcentajePago;
    private int plazo;
    
    public   Cotizacion(){
        this.numCotizacion = 0;
        this.descripcionAuto = "";
        this.precio = 0.0f;
        this.porcentajePago = 0.0f;
        this.plazo = 0;
           
    }

    public Cotizacion(int numCotizacion, String descripcionAuto, float precio, float porcentajePago, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripcionAuto = descripcionAuto;
        this.precio = precio;
        this.porcentajePago = porcentajePago;
        this.plazo = plazo;
    }
    public Cotizacion(Cotizacion otro) {
        this.numCotizacion = this.numCotizacion;
        this.descripcionAuto = this.descripcionAuto;
        this.precio = this.precio;
        this.porcentajePago = this.porcentajePago;
        this.plazo = this.plazo;
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAuto() {
        return descripcionAuto;
    }

    public void setDescripcionAuto(String descripcionAuto) {
        this.descripcionAuto = descripcionAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePago() {
        return porcentajePago;
    }

    public void setPorcentajePago(float porcentajePago) {
        this.porcentajePago = porcentajePago;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
      public float calcularPagoInicial(){
        float pagoInicial =0.0f;
        pagoInicial= this.precio*this.porcentajePago/100f;
        return pagoInicial;
            
    }   
    
    
    public float calcularTotalFinanciar(){
        float total=0.0f;
        total=this.precio - this.calcularPagoInicial();
        return total;
    }
    
    public float calcularPagoMenual(){
        float pagoMensual=0.0f;
        pagoMensual=this.calcularTotalFinanciar()/ this.plazo;
        return pagoMensual;
     
    }
    public void imprimirCotizacion(){
        System.out.println("Numero de cotizacion:"+this.numCotizacion);
        System.out.println("Descripcion  :"+this.descripcionAuto);
        System.out.println("Cual es el precio:"+this.precio);
        System.out.println("Cual es el porcentaje pago inical:"+this.porcentajePago);
        System.out.println("Cuantos plazos :"+this.plazo);
        System.out.println("Su pago inicial es  :"+this.calcularPagoInicial());
        System.out.println("Su monto a financiar seria :"+this.calcularTotalFinanciar());
        System.out.println("su pago mensual es  :"+this.calcularPagoMenual());
        
        
    }
    
}