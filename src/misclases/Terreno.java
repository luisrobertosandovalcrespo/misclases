
package misclases;


public class Terreno {
    private float largo;
    private float ancho;
    
    
    public  Terreno(){
        this.ancho=0.0f;
        this.largo=0.0f;
        
    }     
    public Terreno(float ancho, float largo ){
        this.ancho=ancho;
        this.largo=largo;
        
    
    }
    public Terreno(Terreno x){
        this.ancho=x.ancho;
        this.largo=x.largo;
   
    }
    public void setLargo(float largo){
        this.largo=largo;
        
    
    }
    public void setAncho(float ancho){
        this.ancho=ancho;
        
    }
    public float getLargo(){
        return this.largo;
        
    }
    public float getAncho(){
        return this.ancho;
    }
   
    public float calcularArea(){
        float area=0.0f;
        area=this.largo * this.ancho;
        return area;
        
    }
    public float calcularPerimetro(){
        return this.ancho *2 + this.largo *2;
    }
    public void imprimirTerreno(){
        System.out.println("Lo largo del terreno es "+this.largo);
        System.out.println("Lo ancho del terreno es "+this.ancho);
        System.out.println("El area del terreno es "+this.calcularArea());
        System.out.println("Elperimetro del terreno es "+this.calcularPerimetro());
        
        
    
    }
    
        
}
